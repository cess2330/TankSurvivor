//
//  MainMenuScene.swift
//  TankSurvivor
//
//  Created by Cesar Castillo on 04/04/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import UIKit
import SpriteKit
import Foundation



class MainMenuScene: SKScene {
    
        var is_iphoneX : Bool?
        var tank = SKSpriteNode(imageNamed: "war-tank")
        var tank2 = SKSpriteNode(imageNamed: "tank")
        var roca = SKSpriteNode(imageNamed: "enemy")
        var nave = SKSpriteNode(imageNamed: "enemynave")
        var compras = SKSpriteNode(imageNamed: "compras")
        var ayuda = SKSpriteNode(imageNamed: "ayuda1")
        var ayudaBackground = SKSpriteNode(imageNamed: "pausebackground")
        var OKButton = SKSpriteNode(imageNamed: "done")
        var bomb1 = SKSpriteNode(imageNamed: "bomb")
        var bomb2 = SKSpriteNode(imageNamed: "jetbomb")
        var parachute = SKSpriteNode(imageNamed: "parachute")
        var vida = SKSpriteNode(imageNamed: "supplies")
    
    
        var velocity = CGPoint(x: 300, y: 300)
        var dt : TimeInterval = 0
        let playButton = SKSpriteNode(imageNamed: "playbutton")
        let logo = SKSpriteNode(imageNamed: "tanklogo")
        let central = SKSpriteNode(imageNamed: "central")
        var helicopter = SKSpriteNode(imageNamed: "transport0")
        var jet = SKSpriteNode(imageNamed: "jet")
        var cloud = SKSpriteNode(imageNamed: "clouds")
        var playableArea : CGRect?
    
        let array = ["supplies","bomb","parachute"]
        let arrayJet = ["jetbomb","jetbomb","jetbomb"]
        let playerCategory : UInt32     = 1<<1
        let suppliesCategory : UInt32       = 1<<2
        let bombCategory : UInt32  = 1<<3
        let parachuteCategory : UInt32   = 1<<4
        let bombJetCategory : UInt32 = 1<<5
    
        var background = SKSpriteNode(imageNamed: "background")
    
        var nombreBomb1 : SKLabelNode
        var nombreBomb2 : SKLabelNode
        var nombreParachute : SKLabelNode
        var nombreSupplie : SKLabelNode
        var titulo : SKLabelNode
    
        var ayudaPressed = false
        
        
        override init (size: CGSize) {
            
            
           
            if UIDevice.current.userInterfaceIdiom == .phone {
                if (UIScreen.main.nativeBounds.height == 2436) {
                    is_iphoneX = true
                    let maxAspectRatio : CGFloat = 2.17/1.0
                    let playableHeight = size.width / maxAspectRatio
                    let playableMargin = (size.height - playableHeight) / 2.0
                    playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
                } else {
                    is_iphoneX = false
                    let maxAspectRatio : CGFloat = 16.0/9.0
                    let playableHeight = size.width / maxAspectRatio
                    let playableMargin = (size.height - playableHeight) / 2.0
                    playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
                }
            } else if UIDevice.current.userInterfaceIdiom == .pad {
                
                let maxAspectRatio : CGFloat = 4.0/3.0
                let playableHeight = size.width / maxAspectRatio
                let playableMargin = (size.height - playableHeight) / 2.0
                
                playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
                
            }
            
            self.nombreBomb1 = SKLabelNode(fontNamed: "Army")
            self.nombreBomb2 = SKLabelNode(fontNamed: "Army")
            self.nombreParachute = SKLabelNode(fontNamed: "Army")
            self.nombreSupplie = SKLabelNode(fontNamed: "Army")
            self.titulo = SKLabelNode(fontNamed: "Army")
            
            super.init(size: size)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("El metodo init no ha sido implementado")
        }
        
        override func didMove(to view: SKView) {
            background.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
            background.zPosition = -1
            addChild(background)
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObject),SKAction.wait(forDuration: 1.0)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectJet),SKAction.wait(forDuration: 0.4)])))
            
            
            logo.position = CGPoint(x: size.width/2+300, y: (playableArea?.maxY)!-300)
            logo.zPosition = 2
           
            if (is_iphoneX)! {
                playButton.xScale = 1.0
                playButton.yScale = 1.0
                logo.xScale = 0.5
                logo.yScale = 0.5
                ayudaBackground.xScale = 0.8
                ayudaBackground.yScale = 0.8
                ayudaBackground.size = CGSize(width: self.frame.width/3.5, height:self.frame.height-700)
            } else {
                playButton.xScale = 1.5
                playButton.yScale = 1.5
                logo.xScale = 1.0
                logo.yScale = 1.0
                ayudaBackground.xScale = 1.0
                ayudaBackground.yScale = 1.0
                ayudaBackground.size = CGSize(width: self.frame.width/3, height:self.frame.height-500)
            }
            
            
            
            animateLogo()
            
            tank.position = CGPoint(x: self.frame.width/2-400, y: 400)
            tank.zPosition = 3
            addChild(tank)
            
            tank2.position = CGPoint(x: self.frame.width-200, y: 430)
            tank2.zPosition = 3
            addChild(tank2)
            tank2.run(SKAction.scaleX(to: -1, duration: 0.1))
            
            cloud.position = CGPoint(x: self.frame.width/2-600, y: (playableArea?.maxY)!-200)
            cloud.zPosition = 2
            addChild(cloud)
            
           ayudaBackground.position = CGPoint(x:  self.frame.width/2, y: self.frame.height/2)
           
           
           ayudaBackground.zPosition = 5
            
            
            compras.name = "comprasbutton"
            compras.position = CGPoint(x: self.frame.width/2-700, y: (playableArea?.maxY)!-150)
            compras.zPosition = 3
            compras.xScale = 0.8
            compras.yScale = 0.8
            addChild(compras)
            
            ayuda.name = "ayudabutton"
            ayuda.position = CGPoint(x: self.frame.width/2-900, y: (playableArea?.maxY)!-150)
            ayuda.zPosition = 3
            ayuda.xScale = 0.8
            ayuda.yScale = 0.8
            addChild(ayuda)

            
            
            
            playButton.name = "playbutton"
            playButton.position = CGPoint(x: size.width/2+300, y: (size.height/2)-playButton.size.height)
            playButton.zPosition = 2
         
            
            addChild(logo)
            addChild(playButton)
            
            
            
            //let down = SKAction.moveBy(x: 0.0, y: -size.height/2 + 260, duration: 2.0)
            //central.run(down)
            
            let scaleUP = SKAction.scale(to: 1.0, duration: 0.9)
            logo.run(scaleUP)
            
            //run(SKAction.repeatForever(SKAction.sequence([SKAction.run(logoAppear),SKAction.wait(forDuration: 16.2)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(animateLogo),SKAction.wait(forDuration: 1.0)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(animateLogo),SKAction.wait(forDuration: 2.0)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(animateComprasButton),SKAction.wait(forDuration: 2.0)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(animateAyudaButton),SKAction.wait(forDuration: 2.0)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(animatePlayButton),SKAction.wait(forDuration: 1.6)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemy),SKAction.wait(forDuration: 10)])))
           run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyDistinto),SKAction.wait(forDuration: 5)])))
            
            //playBackgroundMusic(filename: "mainmenumusic.mp3")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAdmod"), object: self)
            
            
        }
    
        func animateComprasButton() {
        
            let moveUp = SKAction.moveBy(x: 0.0, y: 10, duration: 1.0)
            let moveDown = SKAction.moveBy(x: 0.0, y: -10, duration: 1.0)
            compras.run(SKAction.sequence([moveUp,moveDown]))
        }
    
        func animateAyudaButton() {
        
            let moveUp = SKAction.moveBy(x: 0.0, y: 10, duration: 1.0)
            let moveDown = SKAction.moveBy(x: 0.0, y: -10, duration: 1.0)
            ayuda.run(SKAction.sequence([moveUp,moveDown]))
        }


        func animatePlayButton() {
            
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.8)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            playButton.run(fullScale)
        }
        
        func logoAppear() {
            
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.5)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            logo.run(fullScale)
            
        }
        func animateLogo() {
            
            
            //let moveDown = SKAction.moveBy(x: 0.0, y: -150, duration: 5.0)
            let moveUp = SKAction.moveBy(x: 0.0, y: 10, duration: 1.0)
            let moveDown = SKAction.moveBy(x: 0.0, y: -10, duration: 1.0)
            logo.run(SKAction.sequence([moveUp,moveDown]))
            
        }
        
        func animateNave() {
            
            
            let moveUp = SKAction.moveBy(x: 0.0, y: 10, duration: 1.0)
            let moveDown = SKAction.moveBy(x: 0.0, y: -10, duration: 1.0)
            nave.run(SKAction.sequence([moveUp,moveDown]))
            
        }
        
        func animateChimpy() {
            
            
            let moveUp = SKAction.moveBy(x: 0.0, y: 10, duration: 1.0)
            let moveDown = SKAction.moveBy(x: 0.0, y: -10, duration: 1.0)
            nave.run(SKAction.sequence([moveUp,moveDown]))
            
        }
    
    
    
    func createObject() {
        let randomIndex = Int(arc4random_uniform(UInt32(array.count)))
        let randomItem = array[randomIndex]
        
        
        let object = SKSpriteNode(imageNamed: randomItem)
        object.name = randomItem
        object.position = CGPoint(x: helicopter.position.x, y: helicopter.position.y)
        object.xScale = 0.5
        object.yScale = 0.5
        object.zPosition = 1
        addChild(object)
        
        let actionMove = SKAction.moveTo(y: 0.0, duration: 2.0)
        let actionRemove = SKAction.removeFromParent()
        
        object.run(SKAction.sequence([actionMove,actionRemove]))
        
        object.physicsBody = SKPhysicsBody(rectangleOf: object.size)
        
        
        
        switch randomIndex {
        case 0:
            object.physicsBody?.categoryBitMask = suppliesCategory
            break
        case 1:
            object.physicsBody?.categoryBitMask = bombCategory
            break
        case 2:
            object.physicsBody?.categoryBitMask = parachuteCategory
            break
        default:
            break
        }
        
        object.physicsBody?.collisionBitMask = 0
        object.physicsBody?.contactTestBitMask = playerCategory
        object.physicsBody?.affectedByGravity = false
        
    }

    
    
    func createObjectJet() {
        let randomIndex = Int(arc4random_uniform(UInt32(arrayJet.count)))
        let randomItem = arrayJet[randomIndex]
        
        
        let object = SKSpriteNode(imageNamed: randomItem)
        object.name = randomItem
        object.position = CGPoint(x: jet.position.x, y: jet.position.y)
        object.xScale = 0.5
        object.yScale = 0.5
        object.zPosition = 1
        addChild(object)
        
        let actionMove = SKAction.moveTo(y: 0.0, duration: 2.0)
        let actionRemove = SKAction.removeFromParent()
        
        object.run(SKAction.sequence([actionMove,actionRemove]))
        object.physicsBody = SKPhysicsBody(rectangleOf: object.size)
        
        
        
        switch randomIndex {
        case 0:
            object.physicsBody?.categoryBitMask = bombJetCategory
            break
        case 1:
            //object.physicsBody?.categoryBitMask = bombCategory
            break
        case 2:
            //object.physicsBody?.categoryBitMask = parachuteCategory
            break
        default:
            break
        }
        
        object.physicsBody?.collisionBitMask = 0
        object.physicsBody?.contactTestBitMask = playerCategory
        object.physicsBody?.affectedByGravity = false
        
    }

    
    
    
    
        
        func spawnEnemy() {
            let enemy = SKSpriteNode(imageNamed: "transport0.png")
            enemy.name = "enemy"
            enemy.zPosition = 1
            enemy.position = CGPoint(x: size.width + enemy.size.width/2, y: CGFloat.random(min:size.height/2+200, max: (playableArea?.maxY)!-200))
            addChild(enemy)
            
            helicopter = enemy
            run(SKAction.playSoundFileNamed("helicoptersencillo", waitForCompletion: false))
            
            let rotation = SKAction.scaleX(to: -1, duration: 0.1)
            
            let actionTranslation = SKAction.move(to:CGPoint(x: -enemy.size.width/2,y: enemy.position.y), duration: 4.0)
            
            let group = SKAction.group([rotation,actionTranslation])
            
            
            var textures : [SKTexture] = []
            
            for i in 0...4 {
                textures.append(SKTexture(imageNamed: "transport\(i)"))
            }
            
            var helicopterAnimation : SKAction!
            helicopterAnimation = SKAction.animate(with: textures, timePerFrame: 0.02)
            enemy.run(
                
            
                    SKAction.repeatForever(
                        
                        SKAction.sequence([
                            SKAction.repeat(helicopterAnimation, count: 1),
                            SKAction.repeat(helicopterAnimation, count: 1).reversed()
                            ])))

            
            let actionRemove = SKAction.removeFromParent()
            enemy.run(SKAction.sequence([group, actionRemove]))
        }
    
    func spawnEnemyDistinto() {
        let enemy = SKSpriteNode(imageNamed: "jet.png")
        enemy.name = "enemyJet"
        enemy.zPosition = 1
        enemy.position = CGPoint(x: -size.width + enemy.size.width/2, y: CGFloat.random(min:size.height/2+200, max: (playableArea?.maxY)!-200))
        addChild(enemy)
        
        jet = enemy
        run(SKAction.playSoundFileNamed("jetsound", waitForCompletion: false))
        
        let rotation = SKAction.scaleX(to: -1, duration: 0.1)
        
        let actionTranslation = SKAction.move(to:CGPoint(x: (playableArea?.maxX)!+enemy.size.width/2,y: enemy.position.y), duration: 2.5)
        
        let group = SKAction.group([rotation,actionTranslation])
        
        
            let actionRemove = SKAction.removeFromParent()
            enemy.run(SKAction.sequence([group, actionRemove]))
        }
    
    
    func mostrarAyuda() {
        
        if ayudaPressed {
        let changeColorAction = SKAction.colorize(with: SKColor.black, colorBlendFactor: 0.5, duration: 0.1)
    UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionCrossDissolve , animations: {
            
        self.logo.run(changeColorAction)
        self.playButton.run(changeColorAction)
        self.compras.run(changeColorAction)
        self.background.run(changeColorAction)
        self.cloud.run(changeColorAction)

        if (self.is_iphoneX)! {
            
            self.bomb1.position = CGPoint(x:  self.frame.width/2-160, y: self.frame.height/2+280)
            self.bomb1.xScale = 0.4
            self.bomb1.yScale = 0.4
            self.bomb1.zPosition = 6
            
            self.bomb2.position = CGPoint(x:  self.frame.width/2-160, y: self.frame.height/2+150)
            self.bomb2.xScale = 0.4
            self.bomb2.yScale = 0.4
            self.bomb2.zPosition = 6
            
            self.parachute.position = CGPoint(x:  self.frame.width/2-150, y: self.frame.height/2)
            self.parachute.xScale = 0.4
            self.parachute.yScale = 0.4
            self.parachute.zPosition = 6
            
            self.vida.position = CGPoint(x:  self.frame.width/2-150, y: self.frame.height/2-130)
            self.vida.xScale = 0.4
            self.vida.yScale = 0.4
            self.vida.zPosition = 6
            
            self.nombreBomb1.position = CGPoint(x:  self.frame.width/2+50, y: self.frame.height/2+250)
            self.nombreBomb1.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)
            self.nombreBomb1.text = "-1 Live"
            self.nombreBomb1.fontSize = 65
            self.nombreBomb1.zPosition = 6
            
            self.nombreBomb2.position = CGPoint(x:  self.frame.width/2+69, y: self.frame.height/2+130)
            self.nombreBomb2.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)
            self.nombreBomb2.text = "-2 Lives"
            self.nombreBomb2.fontSize = 60
            self.nombreBomb2.zPosition = 6
            
            self.nombreParachute.position = CGPoint(x:  self.frame.width/2+80, y: self.frame.height/2-20)
            self.nombreParachute.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)
            self.nombreParachute.text = "+1 Score"
            self.nombreParachute.fontSize = 60
            self.nombreParachute.zPosition = 6
            
            self.nombreSupplie.position = CGPoint(x:  self.frame.width/2+50, y: self.frame.height/2-170)
            self.nombreSupplie.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)
            self.nombreSupplie.text = "+1 Live"
            self.nombreSupplie.fontSize = 60
            self.nombreSupplie.zPosition = 6
            
            self.OKButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-250)
            self.OKButton.name = "okButton"
            self.OKButton.xScale = 0.4
            self.OKButton.yScale = 0.4
            self.OKButton.zPosition = 6
            
        } else {
            
            self.bomb1.position = CGPoint(x:  self.frame.width/2-200, y: self.frame.height/2+350)
            self.bomb1.xScale = 0.55
            self.bomb1.yScale = 0.55
            self.bomb1.zPosition = 6
            
            self.bomb2.position = CGPoint(x:  self.frame.width/2-200, y: self.frame.height/2+185)
            self.bomb2.xScale = 0.6
            self.bomb2.yScale = 0.6
            self.bomb2.zPosition = 6
            
            self.parachute.position = CGPoint(x:  self.frame.width/2-200, y: self.frame.height/2+10)
            self.parachute.xScale = 0.5
            self.parachute.yScale = 0.5
            self.parachute.zPosition = 6
            
            self.vida.position = CGPoint(x:  self.frame.width/2-200, y: self.frame.height/2-150)
            self.vida.xScale = 0.5
            self.vida.yScale = 0.5
            self.vida.zPosition = 6
            
            self.nombreBomb1.position = CGPoint(x:  self.frame.width/2+40, y: self.frame.height/2+300)
            self.nombreBomb1.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)
            self.nombreBomb1.text = "-1 Live"
            self.nombreBomb1.fontSize = 80
            self.nombreBomb1.zPosition = 6
            
            self.nombreBomb2.position = CGPoint(x:  self.frame.width/2+69, y: self.frame.height/2+150)
            self.nombreBomb2.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)
            self.nombreBomb2.text = "-2 Lives"
            self.nombreBomb2.fontSize = 80
            self.nombreBomb2.zPosition = 6
            
            self.nombreParachute.position = CGPoint(x:  self.frame.width/2+80, y: self.frame.height/2-30)
            self.nombreParachute.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)
            self.nombreParachute.text = "+1 Score"
            self.nombreParachute.fontSize = 80
            self.nombreParachute.zPosition = 6
            
            self.nombreSupplie.position = CGPoint(x:  self.frame.width/2+50, y: self.frame.height/2-210)
            self.nombreSupplie.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)
            self.nombreSupplie.text = "+1 Live"
            self.nombreSupplie.fontSize = 80
            self.nombreSupplie.zPosition = 6
            
            self.OKButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-350)
            self.OKButton.name = "okButton"
            self.OKButton.xScale = 0.8
            self.OKButton.yScale = 0.8
            self.OKButton.zPosition = 6
            
        }
        
        
       
        
        
        self.addChild(self.ayudaBackground)
        self.addChild(self.OKButton)
        self.addChild(self.bomb1)
        self.addChild(self.bomb2)
        self.addChild(self.nombreBomb1)
        self.addChild(self.nombreBomb2)
        self.addChild(self.parachute)
        self.addChild(self.nombreParachute)
        self.addChild(self.vida)
        self.addChild(self.nombreSupplie)
            }, completion: nil)
            
        } else {
            
             let changeColorAction = SKAction.colorize(with: SKColor.white, colorBlendFactor: 1.0, duration: 0.1)
            
         UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionCrossDissolve , animations: {
            
            self.logo.run(changeColorAction)
            self.playButton.run(changeColorAction)
            self.compras.run(changeColorAction)
            self.background.run(changeColorAction)
            self.cloud.run(changeColorAction)
            self.ayudaBackground.removeFromParent()
            self.OKButton.removeFromParent()
            self.bomb1.removeFromParent()
            self.bomb2.removeFromParent()
            self.nombreBomb1.removeFromParent()
            self.nombreBomb2.removeFromParent()
            self.parachute.removeFromParent()
            self.nombreParachute.removeFromParent()
            self.vida.removeFromParent()
            self.nombreSupplie.removeFromParent()
            
            }, completion: nil)
            
        }
        
    }
    
    
    
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            
            let touch = touches.first! as UITouch
            let location = touch.location(in: self)
            let node = self.atPoint(location)
            
            
            if node.name == "playbutton" {
                
                    
                    let levelScene = LevelScene(size: self.size)
                    levelScene.scaleMode = self.scaleMode
                    let transition = SKTransition.push(with: .up, duration: 1.0)
                    self.view?.presentScene(levelScene,transition: transition)
                
            } else if node.name == "comprasbutton" {
                
                
                let scaleUP = SKAction.scale(by: 1.2, duration: 0.25)
                let scaleDown = scaleUP.reversed()
                let fullScale = SKAction.sequence([scaleUP,scaleDown])
                
                compras.run(fullScale, completion: {
                    let comprasScene = ComprasScene(size: self.size)
                    comprasScene.scaleMode = self.scaleMode
                    let transition = SKTransition.push(with: .up, duration: 1.0)
                    self.view?.presentScene(comprasScene,transition: transition)
                    
                })

                
            } else if node.name == "ayudabutton" {
                self.ayuda.name = "none"
                let scaleUP = SKAction.scale(by: 1.2, duration: 0.25)
                let scaleDown = scaleUP.reversed()
                let fullScale = SKAction.sequence([scaleUP,scaleDown])
                
                ayuda.run(fullScale, completion: {
                    
                    self.playButton.name = "none1"
                    self.compras.name = "none2"
                    self.ayudaPressed = true
                    self.mostrarAyuda()
                   

                    
                })

                
            } else if node.name == "okButton" {
                 self.ayuda.name = "ayudabutton"
                let scaleUP = SKAction.scale(by: 1.2, duration: 0.25)
                let scaleDown = scaleUP.reversed()
                let fullScale = SKAction.sequence([scaleUP,scaleDown])
                
                OKButton.run(fullScale, completion: {
               
                self.playButton.name = "playbutton"
                self.compras.name = "comprasbutton"
                self.ayudaPressed = false
                self.mostrarAyuda()
                })
                
            }
            
            
        }
        
        override func update(_ currentTime: TimeInterval) {
            
            
            
            
        }
        
    }


