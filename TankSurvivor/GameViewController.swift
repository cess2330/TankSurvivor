//
//  GameViewController.swift
//  TankSurvivor
//
//  Created by Cesar Castillo on 03/04/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import GameKit
import GoogleMobileAds
import CoreData
import StoreKit


//ca-app-pub-6234971678380165/2872210937

class GameViewController: UIViewController,GKGameCenterControllerDelegate,GADInterstitialDelegate  {

    var iMinSessions = 5
    var iTryAgainSessions = 2
    var nuncaMas = false;
    @IBOutlet var bannerView: GADBannerView!
    var interstitial : GADInterstitial!
    var storeCollection = [StoreItem]()
    var product = [SKProduct]()
    var productS = SKProduct()
    override func viewDidLoad() {
        super.viewDidLoad()
        authPlayer()
        self.bannerView.adUnitID = "ca-app-pub-6234971678380165/2872210937"
        self.bannerView.rootViewController = self
        //let request = GADRequest()
        //request.testDevices = ["4da766ef263c2a310f9c5f3e644930a2"]
         bannerView.load(GADRequest())
        self.interstitial = createAndLoadInterstitial()
        
        NotificationCenter.default.addObserver(self, selector:#selector(showAdmodBanner), name: NSNotification.Name(rawValue: "showAdmod"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(hideAdmodBanner), name: NSNotification.Name(rawValue: "hideAdmod"), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(showInterstitial), name: NSNotification.Name(rawValue: "showInterstitial"), object: nil)
          NotificationCenter.default.addObserver(self, selector:#selector(removeAds), name: NSNotification.Name(rawValue: "removeAds"), object: nil)
        
        

        let hightscoreDefaults = UserDefaults.standard
        if (hightscoreDefaults.value(forKey: "purchased") != nil) {
            if (hightscoreDefaults.value(forKey: "purchased") as! Bool == true)
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeAds"), object: self)
                
                print("Si los removio")
            }
            
        }
        
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            let scene = MainMenuScene(size: CGSize(width: 2048, height: 1536))
            // Set the scale mode to scale to fit the window
            scene.scaleMode = .aspectFill
            
            // Present the scene
            view.presentScene(scene)
            
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = false
            view.showsNodeCount = false
            
        }
        
    }
    
    
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    func authPlayer() {
        let localPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {
            (view, error) in
            
            if view != nil {
                self.present(view!, animated: true, completion: nil)
            } else {
                print(GKLocalPlayer.localPlayer().isAuthenticated)
            }
            
        }
    }
    
    func saveHighscore(number:Int) {
        if GKLocalPlayer.localPlayer().isAuthenticated {
            let scoreReporter = GKScore(leaderboardIdentifier: "TankSurvivor")
            scoreReporter.value = Int64(number)
            let scoreArray : [GKScore] = [scoreReporter]
            
            GKScore.report(scoreArray, withCompletionHandler: nil)
        }
        
    }
    
    func showAdmodBanner() {
        self.bannerView.isHidden = false
        
    }
    
    func hideAdmodBanner() {
        self.bannerView.isHidden = true
    }
    
    func removeAds() {
        
       hideAdmodBanner()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cambiarImagen"), object: self)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showAdmod"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "hideAdmod"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "showInterstitial"), object: nil)
    }

    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-6234971678380165/1418314739")
        interstitial.delegate = self
        //let request = GADRequest()
        // request.testDevices = ["6f3f7014e9128f5a622633bb535b7c27"]
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        self.interstitial = createAndLoadInterstitial()
    }
    
    func showInterstitial() {
        if self.interstitial.isReady {
            self.interstitial.present(fromRootViewController: self)
        }
    }

    func showLeaderBoard() {
        let VC = self.view.window?.rootViewController
        let gvc = GKGameCenterViewController()
        
        gvc.gameCenterDelegate = self
        
        VC?.present(gvc, animated: true, completion: nil)
    }
    
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
    

}
