//
//  RandomUtils.swift
//  SpaceChimpy
//
//  Created by Cesar Castillo on 07/03/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import Foundation
import CoreGraphics

extension CGFloat {
    //Devuelve un numero aleatorio entre 0 y 1
    static func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / Float(UInt32.max))
    }
    //Devuelve un numero aleatorio entre el min y el max
    static func random(min:CGFloat, max: CGFloat) -> CGFloat {
        assert(min < max)
        return CGFloat.random() * (max - min) + min
    }
}
