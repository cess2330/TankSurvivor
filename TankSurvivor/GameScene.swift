//
//  GameScene.swift
//  TankSurvivor
//
//  Created by Cesar Castillo on 03/04/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import SpriteKit
import GameplayKit
import AudioToolbox


class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var tank : SKSpriteNode!
    var helicopter : SKSpriteNode!
    var background : SKSpriteNode!
    var pauseButton : SKSpriteNode!

    var tipo : String
    var playableArea : CGRect?
    var isInvisible : Bool
    var isGameOver = false
    var pausePresionado = false
    var tankLives = 0
    var score = 0
    var highscore = 0
    var livesLabel : SKLabelNode
    var scoreLabel : SKLabelNode
    let highLabel : SKLabelNode
    
    
    var jet = SKSpriteNode(imageNamed: "jet")
    var superJet = SKSpriteNode(imageNamed: "jet")
    var plane = SKSpriteNode(imageNamed: "plane")
    let array = ["bomb","parachute"]
    let arrayJet = ["jetbomb","jetbomb"]
    let arrayPlane = ["supplies","supplies"]
    let arraySuperJet = ["bomb","bomb"]
    let playerCategory : UInt32     = 1<<1
    let suppliesCategory : UInt32       = 1<<2
    let bombCategory : UInt32  = 1<<3
    let parachuteCategory : UInt32   = 1<<4
    let bombJetCategory : UInt32 = 1<<5
    let bombSuperJetCategory : UInt32 = 1<<6
    
    var gameOverSprite = SKSpriteNode(imageNamed: "gameoverbutton")
    var cloud = SKSpriteNode(imageNamed: "cloud")
    var clouds2 = SKSpriteNode(imageNamed: "cloud")
    var pauseNodeBackground = SKSpriteNode(imageNamed: "pausebackground")
    var resumeButton = SKSpriteNode(imageNamed: "resumebutton")
    var menuButton = SKSpriteNode(imageNamed: "menubutton")
    var leftButton = SKSpriteNode(imageNamed: "leftbutton")
    var rightButton = SKSpriteNode(imageNamed: "rightbutton")
    var highscorePause = SKSpriteNode(imageNamed: "highscore")
    
    var is_iphoneX : Bool?
    
    
    init (size: CGSize,tipo: String) {
        
        self.tipo = tipo
        
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            
            if (UIScreen.main.nativeBounds.height == 2436) {
                is_iphoneX = true
                let maxAspectRatio : CGFloat = 2.17/1.0
                let playableHeight = size.width / maxAspectRatio
                let playableMargin = (size.height - playableHeight) / 2.0
                playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
            } else {
                 is_iphoneX = false
                let maxAspectRatio : CGFloat = 16.0/9.0
                let playableHeight = size.width / maxAspectRatio
                let playableMargin = (size.height - playableHeight) / 2.0
                playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
            }
        } else if UIDevice.current.userInterfaceIdiom == .pad {
            
            let maxAspectRatio : CGFloat = 4.0/3.0
            let playableHeight = size.width / maxAspectRatio
            let playableMargin = (size.height - playableHeight) / 2.0
            
            playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
            
        }
        
        isInvisible = false
        
        pauseButton = SKSpriteNode(imageNamed: "pausebutton")
        
        
        livesLabel = SKLabelNode(fontNamed: "Army")
        scoreLabel = SKLabelNode(fontNamed: "Army")
        highLabel = SKLabelNode(fontNamed: "Army")

        
        
        super.init(size: size)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("El metodo init no ha sido implementado")
    }

    
    override func didMove(to view: SKView) {
        
        initHelicopter()
        initBackground()
        initTank()
        
       
        
        NotificationCenter.default.addObserver(self, selector:#selector(pauseGame), name: NSNotification.Name(rawValue: "pauseGame"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "hideAdmod"), object: self)
        

        
        physicsWorld.contactDelegate = self
        
        cloud.position = CGPoint(x: self.frame.width/2-700, y: (playableArea?.maxY)!-200)
        cloud.zPosition = 1
        addChild(cloud)
        
        
        clouds2.position = CGPoint(x: self.frame.width/2+700, y: (playableArea?.maxY)!-200)
        clouds2.zPosition = 1
        addChild(clouds2)
        
        pauseButton.position = CGPoint(x: size.width/2, y: (playableArea?.maxY)!-90)
        pauseButton.zPosition = 1
        pauseButton.xScale = 0.5
        pauseButton.yScale = 0.5
        pauseButton.name = "pausebutton"
        pauseButton.isHidden = false
        addChild(pauseButton)
        
        livesLabel.position = CGPoint(x: 50.0, y: (playableArea?.maxY)!-50)
        livesLabel.fontSize = 100
        livesLabel.zPosition = 2
        livesLabel.verticalAlignmentMode = .top
        livesLabel.horizontalAlignmentMode = .left
        addChild(livesLabel)

        scoreLabel.position = CGPoint(x:(playableArea?.maxX)!-100.0, y: (playableArea?.maxY)!-50)
        scoreLabel.fontSize = 100
        scoreLabel.zPosition = 2
        scoreLabel.verticalAlignmentMode = .top
        scoreLabel.horizontalAlignmentMode = .right
        addChild(scoreLabel)
        
        
        leftButton.name = "leftbutton"
        leftButton.position = CGPoint(x: (playableArea?.minX)!+200, y: (playableArea?.minY)!+200)
        leftButton.zPosition = 2
        leftButton.xScale = 1.0
        leftButton.yScale = 1.0
        leftButton.alpha = 0.8
        leftButton.isHidden = false
        addChild(leftButton)
        
        
        rightButton.name = "rightbutton"
        rightButton.position = CGPoint(x: (playableArea?.maxX)!-200, y: (playableArea?.minY)!+200)
        rightButton.zPosition = 2
        rightButton.xScale = 1.0
        rightButton.yScale = 1.0
        rightButton.isHidden = false
        rightButton.alpha = 0.8
        addChild(rightButton)
        
        

       
        
        
       
        
        if self.tipo == "easy" {
            tankLives = 5
             run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObject),SKAction.wait(forDuration: 1.0)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectPlane),SKAction.wait(forDuration: 2.0)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnSuppliesPlane),SKAction.wait(forDuration: 30.0)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectJet),SKAction.wait(forDuration: 0.8)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyJet),SKAction.wait(forDuration: 6)])))
            
            let hightscoreDefaults = UserDefaults.standard
            if (hightscoreDefaults.value(forKey: "Highscoreeasy") != nil) {
                highscore = hightscoreDefaults.value(forKey: "Highscoreeasy") as! NSInteger
            }
            
        } else if self.tipo == "normal" {
            tankLives = 3
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObject),SKAction.wait(forDuration: 1.0)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectJet),SKAction.wait(forDuration: 0.5)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyJet),SKAction.wait(forDuration: 5)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectPlane),SKAction.wait(forDuration: 2.5)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnSuppliesPlane),SKAction.wait(forDuration: 10.0)])))
            
            let hightscoreDefaults = UserDefaults.standard
            if (hightscoreDefaults.value(forKey: "Highscorenormal") != nil) {
                highscore = hightscoreDefaults.value(forKey: "Highscorenormal") as! NSInteger
            }
            
        } else if self.tipo == "hard" {
            
            tankLives = 3
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObject),SKAction.wait(forDuration: 1.0)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectJet),SKAction.wait(forDuration: 0.5)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyJet),SKAction.wait(forDuration: 3)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectPlane),SKAction.wait(forDuration: 1.0)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnSuppliesPlane),SKAction.wait(forDuration: 15.0)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectSuperJet),SKAction.wait(forDuration: 0.25)])))
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemySuperJet),SKAction.wait(forDuration: 50.0)])))
            
            let hightscoreDefaults = UserDefaults.standard
            if (hightscoreDefaults.value(forKey: "Highscorehard") != nil) {
                highscore = hightscoreDefaults.value(forKey: "Highscorehard") as! NSInteger
            }
            
        }
        
        
    }
    
    func initBackground() {
        
        background = SKSpriteNode(imageNamed: "background")
        background.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
        background.zPosition = -1
        addChild(background)
    }
    
    func initTank() {
        
        if tipo == "easy" {
            tank = SKSpriteNode(imageNamed: "war-tank")
            tank.position = CGPoint(x: self.frame.width/2, y: 400)
            
        } else if tipo == "normal" {
            
            tank = SKSpriteNode(imageNamed: "tank")
            tank.position = CGPoint(x: self.frame.width/2, y: 430)
            
        } else if tipo == "hard" {
            
            tank = SKSpriteNode(imageNamed: "tank")
            tank.position = CGPoint(x: self.frame.width/2, y: 430)
            
        }
        
        
        tank.zPosition = 1
        addChild(tank)
        
        tank.physicsBody = SKPhysicsBody(rectangleOf: tank.size)
        tank.physicsBody?.categoryBitMask = playerCategory
        tank.physicsBody?.collisionBitMask = 0
        tank.physicsBody?.affectedByGravity = false
        tank.physicsBody?.allowsRotation = false
        
        
    }
    
    func initHelicopter() {
        var duration : TimeInterval = 4.0
        helicopter = SKSpriteNode(imageNamed: "transport0")
        helicopter.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+300)
        helicopter.zPosition = 2
        addChild(helicopter)
        
        let helicopterSound = SKAudioNode(fileNamed: "helicopter")
        self.addChild(helicopterSound)
        
        
        if tipo == "easy"{
            duration = 4.0
            helicopter.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+300)
        } else if tipo == "normal" {
            duration = 3.0
            helicopter.position = CGPoint(x: self.frame.width/2-250, y: self.frame.height/2+300)
            
        } else if tipo == "hard" {
            
            duration = 3.0
            helicopter.position = CGPoint(x: -self.frame.width/2+20, y: self.frame.height/2+300)
        }
        
        var textures : [SKTexture] = []
        
        for i in 0...4 {
            textures.append(SKTexture(imageNamed: "transport\(i)"))
        }
        
        var helicopterAnimation : SKAction!
        helicopterAnimation = SKAction.animate(with: textures, timePerFrame: 0.015)
        helicopter.run(//SKAction.repeatForever(helicopterAnimation))
            
            SKAction.group([
                SKAction.repeatForever(
                    
                    SKAction.sequence([
                        SKAction.repeat(helicopterAnimation, count: 1),
                        SKAction.repeat(helicopterAnimation, count: 1).reversed()
                        ])
                ),
                
                
                SKAction.repeatForever(
                    SKAction.sequence([
                        
                        
                        SKAction.moveTo(x: self.size.width-100, duration: duration),
                        SKAction.scaleX(to: -1, duration: 0.1),
                        SKAction.moveTo(x: 100, duration: duration),
                        SKAction.scaleX(to: 1, duration: 0.1)
                        
                        
                        ])
                    
                )
                ]
            )
        )
        
    }
    
   
    
    func removeObject(contact: SKPhysicsBody) {
        tank.run(
            SKAction.sequence([
                
                SKAction.wait(forDuration: 0.2),
                SKAction.run({
                    contact.node?.removeFromParent()
                })
                
                
                ])
            
        )
    }
    
    
    func createObject() {
        let randomIndex = Int(arc4random_uniform(UInt32(array.count)))
        let randomItem = array[randomIndex]
        
        
        let object = SKSpriteNode(imageNamed: randomItem)
        object.name = randomItem
        object.position = CGPoint(x: helicopter.position.x, y: helicopter.position.y)
        object.xScale = 0.5
        object.yScale = 0.5
        object.zPosition = 1
        addChild(object)
        
        let actionMove = SKAction.moveTo(y: 0.0, duration: 2.0)
        let actionRemove = SKAction.removeFromParent()
        
        object.run(SKAction.sequence([actionMove,actionRemove]))
        
        object.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: object.size.width/2, height: object.size.height/2))
        
        
        
        switch randomIndex {
        case 0:
            object.physicsBody?.categoryBitMask = bombCategory
            break
        case 1:
             object.physicsBody?.categoryBitMask = parachuteCategory
            break
        default:
            break
        }
        
        object.physicsBody?.collisionBitMask = 0
        object.physicsBody?.contactTestBitMask = playerCategory
        object.physicsBody?.affectedByGravity = false
        
    }
    
    
    func createObjectJet() {
        let randomIndex = Int(arc4random_uniform(UInt32(arrayJet.count)))
        let randomItem = arrayJet[randomIndex]
        
        
        let object = SKSpriteNode(imageNamed: randomItem)
        object.name = randomItem
        object.position = CGPoint(x: jet.position.x, y: jet.position.y)
        object.xScale = 0.6
        object.yScale = 0.6
        object.zPosition = 1
        addChild(object)
        
        let actionMove = SKAction.moveTo(y: 0.0, duration: 2.0)
        let actionRemove = SKAction.removeFromParent()
        
        object.run(SKAction.sequence([actionMove,actionRemove]))
        object.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: object.size.width/2, height: object.size.height/2))
        

        
        
        
        switch randomIndex {
        case 0:
            object.physicsBody?.categoryBitMask = bombJetCategory
            break
        case 1:
            object.physicsBody?.categoryBitMask = bombJetCategory
            break
        default:
            break
        }
        
        object.physicsBody?.collisionBitMask = 0
        object.physicsBody?.contactTestBitMask = playerCategory
        object.physicsBody?.affectedByGravity = false
        
    }
    
    func createObjectSuperJet() {
        let randomIndex = Int(arc4random_uniform(UInt32(arraySuperJet.count)))
        let randomItem = arraySuperJet[randomIndex]
        
        
        let object = SKSpriteNode(imageNamed: randomItem)
        object.name = randomItem
        object.position = CGPoint(x: superJet.position.x, y: superJet.position.y)
        object.xScale = 0.6
        object.yScale = 0.6
        object.zPosition = 1
        addChild(object)
        
        let actionMove = SKAction.moveTo(y: 0.0, duration: 2.0)
        let actionRemove = SKAction.removeFromParent()
        
        object.run(SKAction.sequence([actionMove,actionRemove]))
        object.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: object.size.width/2, height: object.size.height/2))
        
        
        
        
        
        switch randomIndex {
        case 0:
            object.physicsBody?.categoryBitMask = bombSuperJetCategory
            break
        case 1:
            object.physicsBody?.categoryBitMask = bombSuperJetCategory
            break
        default:
            break
        }
        
        object.physicsBody?.collisionBitMask = 0
        object.physicsBody?.contactTestBitMask = playerCategory
        object.physicsBody?.affectedByGravity = false
        
    }

    
    func createObjectPlane() {
        let randomIndex = Int(arc4random_uniform(UInt32(arrayPlane.count)))
        let randomItem = arrayPlane[randomIndex]
        
        
        let object = SKSpriteNode(imageNamed: randomItem)
        object.name = randomItem
        object.position = CGPoint(x: plane.position.x, y: plane.position.y)
        object.xScale = 0.5
        object.yScale = 0.5
        object.zPosition = 1
        addChild(object)
        
        let actionMove = SKAction.moveTo(y: 0.0, duration: 2.0)
        
        let actionRemove = SKAction.removeFromParent()
        
        object.run(SKAction.sequence([actionMove,actionRemove]))
        object.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: object.size.width/2, height: object.size.height/2))
        

        switch randomIndex {
        case 0:
            object.physicsBody?.categoryBitMask = suppliesCategory
            break
        case 1:
            object.physicsBody?.categoryBitMask = suppliesCategory
            break
        default:
            break
        }
        
        object.physicsBody?.collisionBitMask = 0
        object.physicsBody?.contactTestBitMask = playerCategory
        object.physicsBody?.affectedByGravity = false
        
    }


    
    func didBegin(_ contact: SKPhysicsContact) {
        let contact = contact.bodyA.categoryBitMask == playerCategory ? contact.bodyB : contact.bodyA
        
        if contact.categoryBitMask == bombCategory {
            removeObject(contact: contact)
            shakeCamera(duration: 1.5)
            tankLives -= 1
            run(SKAction.playSoundFileNamed("bomb", waitForCompletion: false))
            updateHUD()
            
        } else if contact.categoryBitMask == suppliesCategory {
            removeObject(contact: contact)
            tankLives += 1
            run(SKAction.playSoundFileNamed("pop", waitForCompletion: false))
            updateHUD()
            
            
        } else if contact.categoryBitMask == parachuteCategory {
            removeObject(contact: contact)
            score += 1
            run(SKAction.playSoundFileNamed("pop", waitForCompletion: false))
            updateHUD()
            
        } else if contact.categoryBitMask == bombJetCategory {
            removeObject(contact: contact)
            shakeCamera(duration: 1.5)
            tankLives -= 2
            run(SKAction.playSoundFileNamed("bomb", waitForCompletion: false))
            updateHUD()
            
        } else if contact.categoryBitMask == bombSuperJetCategory {
            removeObject(contact: contact)
            shakeCamera(duration: 1.5)
            tankLives -= 1
            run(SKAction.playSoundFileNamed("bomb", waitForCompletion: false))
            updateHUD()
           
        }
        
    }
    
    func shakeCamera(duration:CGFloat) {
        let amplitudeX : CGFloat = 20
        let amplitudeY : CGFloat = 20
        let numberOfShakes = duration / 0.04
        var actionsArray : [SKAction] = []
        for _ in 1...Int(numberOfShakes) {
           
            let moveX = CGFloat(arc4random_uniform(UInt32(amplitudeX))) - amplitudeX/2
            let moveY = CGFloat(arc4random_uniform(UInt32(amplitudeY))) - amplitudeY/2
            let shakeAction = SKAction.moveBy(x: moveX, y: moveY, duration: 0.02)
            shakeAction.timingMode = SKActionTimingMode.easeOut
            actionsArray.append(shakeAction)
            actionsArray.append(shakeAction.reversed())
            
        }
        let actionSeq = SKAction.sequence(actionsArray)
        let vibrateAction = SKAction.repeat(SKAction.run(vibrate), count:5)
        background.run(actionSeq)
        run(vibrateAction)
    }
    
    
    
    
    
    
    func spawnSuppliesPlane() {
        let plane = SKSpriteNode(imageNamed: "plane.png")
        plane.name = "plane"
        plane.zPosition = 1
        
        run(SKAction.playSoundFileNamed("plane", waitForCompletion: false))
        
        
        self.plane = plane
        
        if tipo == "easy" {
            
            
            plane.position = CGPoint(x: size.width + plane.size.width/2, y: CGFloat.random(min:size.height/2+200, max: (playableArea?.maxY)!-100))
            addChild(plane)
            
            let actionTranslation = SKAction.move(to:CGPoint(x: -(playableArea?.maxX)!+plane.size.width/2,y: plane.position.y), duration: 5.0)
            let actionRemove = SKAction.removeFromParent()
            plane.run(SKAction.sequence([actionTranslation, actionRemove]))
            
            
        } else if tipo == "normal" {
            
            
            plane.position = CGPoint(x: size.width + plane.size.width/2, y: CGFloat.random(min:size.height/2+200, max: (playableArea?.maxY)!-100))
            addChild(plane)
            
            let actionTranslation = SKAction.move(to:CGPoint(x: -(playableArea?.maxX)!+plane.size.width/2,y: plane.position.y), duration: 5.0)
            let actionRemove = SKAction.removeFromParent()
            plane.run(SKAction.sequence([actionTranslation, actionRemove]))

            
        } else if tipo == "hard" {
            
            plane.position = CGPoint(x: -size.width + plane.size.width/2, y: CGFloat.random(min:size.height/2+200, max: (playableArea?.maxY)!-100))
            addChild(plane)
            
            let rotation = SKAction.scaleX(to: -1, duration: 0.1)
            let actionTranslation = SKAction.move(to:CGPoint(x: (playableArea?.maxX)!+plane.size.width/2,y: plane.position.y), duration: 3.0)
            
            let group = SKAction.group([rotation,actionTranslation])
            
            
            let actionRemove = SKAction.removeFromParent()
            plane.run(SKAction.sequence([group, actionRemove]))
        
            
            
          }
        
    }

    
    
    func spawnEnemyJet() {
        let enemy = SKSpriteNode(imageNamed: "jet.png")
        enemy.name = "enemyJet"
        enemy.zPosition = 1
     
        
        jet = enemy
        run(SKAction.playSoundFileNamed("jetsound", waitForCompletion: false))
        
        if tipo == "easy" {
            
            enemy.position = CGPoint(x: size.width + enemy.size.width/2, y: CGFloat.random(min:size.height/2+200, max: (playableArea?.maxY)!-100))
            addChild(enemy)
            
            let actionTranslation = SKAction.move(to:CGPoint(x: -(playableArea?.maxX)!+enemy.size.width/2,y: enemy.position.y), duration: 4.0)
            let actionRemove = SKAction.removeFromParent()
            enemy.run(SKAction.sequence([actionTranslation, actionRemove]))
            
        } else if tipo == "normal" {
            
            enemy.position = CGPoint(x: -size.width + enemy.size.width/2, y: CGFloat.random(min:size.height/2+200, max: (playableArea?.maxY)!-100))
               addChild(enemy)

            let rotation = SKAction.scaleX(to: -1, duration: 0.1)
            let actionTranslation = SKAction.move(to:CGPoint(x: (playableArea?.maxX)!+enemy.size.width/2,y: enemy.position.y), duration: 2.0)
            
            let group = SKAction.group([rotation,actionTranslation])
            
            
            let actionRemove = SKAction.removeFromParent()
            enemy.run(SKAction.sequence([group, actionRemove]))
            
        } else if tipo == "hard" {
            
            enemy.position = CGPoint(x: size.width + enemy.size.width/2, y: CGFloat.random(min:size.height/2+200, max: (playableArea?.maxY)!-100))
            addChild(enemy)

            let actionTranslation = SKAction.move(to:CGPoint(x: -(playableArea?.maxX)!+enemy.size.width/2,y: enemy.position.y), duration: 2.0)
            let actionRemove = SKAction.removeFromParent()
            enemy.run(SKAction.sequence([actionTranslation, actionRemove]))
        }
        
    }
    

    
    func spawnEnemySuperJet() {
        let enemy = SKSpriteNode(imageNamed: "jet.png")
        enemy.name = "enemySuperJet"
        enemy.zPosition = 1
        
        
        superJet = enemy
        run(SKAction.playSoundFileNamed("jetsound", waitForCompletion: false))
        
      if tipo == "hard" {
            
            enemy.position = CGPoint(x: -size.width + enemy.size.width/2, y: CGFloat.random(min:size.height/2+200, max: (playableArea?.maxY)!-100))
            addChild(enemy)
            
            let rotation = SKAction.scaleX(to: -1, duration: 0.1)
            let actionTranslation = SKAction.move(to:CGPoint(x: (playableArea?.maxX)!+enemy.size.width/2,y: enemy.position.y), duration: 2.0)
            
            let group = SKAction.group([rotation,actionTranslation])
            
            
            let actionRemove = SKAction.removeFromParent()
            enemy.run(SKAction.sequence([group, actionRemove]))
        }
        
        
        
        
    }
    

    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch  = touches.first
        let touchLocation = touch?.location(in: self)
        
        let touch2 = touches.first! as UITouch
        let location2 = touch2.location(in: self)
        let node = self.atPoint(location2)
        
        
        if tank.position.x > (touchLocation?.x)! {
            if node.name == "leftbutton"{
            tank.run(SKAction.scaleX(to: -1, duration: 0.09))
                
                let xDist = (tank.position.x-(touchLocation?.x)!)
                let yDist = (tank.position.y-(touchLocation?.y)!)
                
                let distance = sqrt((xDist*xDist) + (yDist*yDist))
                
                let velocity : CGFloat = 500.0
                let duration : CGFloat = distance/velocity
                
                tank.removeAction(forKey: "moveTank")
                
                tank.run(SKAction.moveTo(x: (touchLocation?.x)!, duration: TimeInterval(duration)), withKey:"moveTank")
            }
        } else {
            if node.name == "rightbutton" {
            tank.run(SKAction.scaleX(to: 1, duration: 0.09))
                
                let xDist = (tank.position.x-(touchLocation?.x)!)
                let yDist = (tank.position.y-(touchLocation?.y)!)
                
                let distance = sqrt((xDist*xDist) + (yDist*yDist))
                
                let velocity : CGFloat = 500.0
                let duration : CGFloat = distance/velocity
                
                tank.removeAction(forKey: "moveTank")
                
                tank.run(SKAction.moveTo(x: (touchLocation?.x)!, duration: TimeInterval(duration)), withKey:"moveTank")
            }
        }
        
      
        
        
        if node.name == "pausebutton" {
            
            if (self.isPaused == false) {
                
                pauseGame()
                pausePresionado = true
            }
            
        } else if node.name == "menu" {
            if (!is_iphoneX!) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showInterstitial"), object: self)
            }
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    appDelegate.showReview()
    let mainMenu = MainMenuScene(size: self.size)
    mainMenu.scaleMode = self.scaleMode
    let transition = SKTransition.reveal(with: .up, duration: 1.0)
    self.view?.presentScene(mainMenu, transition: transition)
        } else if node.name == "resume" {
            
            
            if tipo == "easy" {
               touchResume()
                
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObject),SKAction.wait(forDuration: 1.0)])))
                
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectPlane),SKAction.wait(forDuration: 1.0)])))
                
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnSuppliesPlane),SKAction.wait(forDuration: 15.0)])))
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectJet),SKAction.wait(forDuration: 0.8)])))
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyJet),SKAction.wait(forDuration: 6)])))

                
            } else if tipo == "normal" {
               touchResume()
                
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObject),SKAction.wait(forDuration: 1.0)])))
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectJet),SKAction.wait(forDuration: 0.5)])))
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyJet),SKAction.wait(forDuration: 5)])))
                
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectPlane),SKAction.wait(forDuration: 0.5)])))
                
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnSuppliesPlane),SKAction.wait(forDuration: 8.0)])))

                
            } else if tipo == "hard" {
                touchResume()
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObject),SKAction.wait(forDuration: 1.0)])))
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectJet),SKAction.wait(forDuration: 0.5)])))
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyJet),SKAction.wait(forDuration: 3)])))
                
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(createObjectPlane),SKAction.wait(forDuration: 2.0)])))
                
                run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnSuppliesPlane),SKAction.wait(forDuration: 15.0)])))
                
            }
            
        }


    }
    
    func touchResume() {
        
        self.isPaused = false
        self.pauseButton.isHidden = false
        self.rightButton.isHidden = false
        self.leftButton.isHidden = false
        pauseNodeBackground.removeFromParent()
        resumeButton.removeFromParent()
        menuButton.removeFromParent()
        highscorePause.removeFromParent()
        pauseNodeBackground.removeFromParent()
        highLabel.removeFromParent()
        
    }
    
    
    func pauseGame() {
        
        
        if (!isPaused) {
            self.run(SKAction.playSoundFileNamed("pause", waitForCompletion: false), completion: {
                
                if (self.is_iphoneX)! {
                    self.pauseNodeBackground.size = CGSize(width: self.frame.width/3.5, height: self.frame.height-700)
                    
                    self.pauseNodeBackground.position = CGPoint(x:  self.frame.width/2, y: self.frame.height/2)
                    
                    //pauseNodeBackground.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                    self.pauseNodeBackground.zPosition = 3
                    
                    self.menuButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-150)//(x: self.pauseNodeBackground.size.width/60, y: -self.pauseNodeBackground.size.height/3)
                    self.menuButton.name = "menu"
                    self.menuButton.zPosition = 4
                    self.menuButton.xScale = 0.8
                    self.menuButton.yScale = 0.8
                    
                    self.resumeButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-280)//(x: self.pauseNodeBackground.size.width/60, y: -self.pauseNodeBackground.size.height/6)
                    self.resumeButton.name = "resume"
                    self.resumeButton.zPosition = 4
                    self.resumeButton.xScale = 0.8
                    self.resumeButton.yScale = 0.8
                    
                    
                    if self.tipo == "easy" {
                        self.highscorePause = SKSpriteNode(imageNamed: "highscoreeasy")
                    } else if self.tipo == "normal" {
                        self.highscorePause = SKSpriteNode(imageNamed: "highscorenormal")
                    } else if self.tipo == "hard" {
                        self.highscorePause = SKSpriteNode(imageNamed: "highscorehard")
                    }
                    
                    self.highscorePause.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+225)
                    self.highscorePause.zPosition = 4
                    self.highscorePause.xScale = 0.9
                    self.highscorePause.yScale = 0.9
                    self.highLabel.fontSize = 180
                       self.highLabel.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-50)
                    
                } else {
                     self.pauseNodeBackground.size = CGSize(width: self.frame.width/3, height: self.frame.height-500)
                    self.pauseNodeBackground.position = CGPoint(x:  self.frame.width/2, y: self.frame.height/2)
                    
                    //pauseNodeBackground.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                    self.pauseNodeBackground.zPosition = 3
                    
                    self.menuButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-180)//(x: self.pauseNodeBackground.size.width/60, y: -self.pauseNodeBackground.size.height/3)
                    self.menuButton.name = "menu"
                    self.menuButton.zPosition = 4
                    
                    self.resumeButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-350)//(x: self.pauseNodeBackground.size.width/60, y: -self.pauseNodeBackground.size.height/6)
                    self.resumeButton.name = "resume"
                    self.resumeButton.zPosition = 4
                    
                    
                    if self.tipo == "easy" {
                        self.highscorePause = SKSpriteNode(imageNamed: "highscoreeasy")
                    } else if self.tipo == "normal" {
                        self.highscorePause = SKSpriteNode(imageNamed: "highscorenormal")
                    } else if self.tipo == "hard" {
                        self.highscorePause = SKSpriteNode(imageNamed: "highscorehard")
                    }
                    
                    self.highscorePause.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+280)
                    self.highscorePause.zPosition = 4
                    self.highscorePause.xScale = 1.2
                    self.highscorePause.yScale = 1.2
                    self.highLabel.fontSize = 230
                       self.highLabel.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-75)
                }
                
                
                
                
                self.addChild(self.pauseNodeBackground)
                self.addChild(self.resumeButton)
                self.addChild(self.menuButton)
                self.addChild(self.highscorePause)
            
             
                
                self.highLabel.text = "\(self.highscore)"
                self.highLabel.zPosition = 4
                self.highLabel.fontColor = #colorLiteral(red: 0.07058823529, green: 0.2470588235, blue: 0.2941176471, alpha: 1)
                self.addChild(self.highLabel)
                //backgroundAudioPlayer.pause()
                self.isInvisible = true
                self.isPaused = true
                self.pauseButton.isHidden = true
                self.rightButton.isHidden = true
                self.leftButton.isHidden = true
                self.removeAllActions()
                
            })
        }
    
        
            
        }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        updateHUD()
        if tankLives <= 0 && !isGameOver {
            isGameOver = true
            updateHUD()
            if score > highscore {
                highscore = score
                
                
                if tipo == "easy" {
                    
                    let hightscoreDefaults = UserDefaults.standard
                    hightscoreDefaults.set(highscore, forKey: "Highscoreeasy")
                    hightscoreDefaults.synchronize()
                    
                    print(highscore)
                } else if tipo == "normal" {
                    
                    let hightscoreDefaults = UserDefaults.standard
                    hightscoreDefaults.set(highscore, forKey: "Highscorenormal")
                    hightscoreDefaults.synchronize()
                    
                } else if tipo == "hard" {
                    
                    let hightscoreDefaults = UserDefaults.standard
                    hightscoreDefaults.set(highscore, forKey: "Highscorehard")
                    hightscoreDefaults.synchronize()
                    
                }
            }
            
            
            
            self.gameOverSprite.position = CGPoint(x: size.width/2, y: size.height/2)
            self.gameOverSprite.xScale = 0.8
            self.gameOverSprite.yScale = 0.8
            self.gameOverSprite.zPosition = 3
            addChild(gameOverSprite)
            

            pauseButton.isHidden = true
            self.rightButton.isHidden = true
            self.leftButton.isHidden = true
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.8)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            let repeatAction = SKAction.repeat(fullScale, count: 2)
            
            
            gameOverSprite.run(repeatAction, completion: {
                
                self.isPaused = true
                self.removeAllActions()
                self.gameOverSprite.removeFromParent()
                
                if (self.is_iphoneX)! {
                    
                    self.pauseNodeBackground.size = CGSize(width: self.frame.width/3.5, height: self.frame.height-700)
                    
                    self.pauseNodeBackground.position = CGPoint(x:  self.frame.width/2, y: self.frame.height/2)
                    
                    //pauseNodeBackground.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                    self.pauseNodeBackground.zPosition = 3
                    
                    self.menuButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-150)//(x: self.pauseNodeBackground.size.width/60, y: -self.pauseNodeBackground.size.height/3)
                    self.addChild(self.pauseNodeBackground)
                    
                    
                    if self.tipo == "easy" {
                        self.highscorePause = SKSpriteNode(imageNamed: "highscoreeasy")
                    } else if self.tipo == "normal" {
                        self.highscorePause = SKSpriteNode(imageNamed: "highscorenormal")
                    } else if self.tipo == "hard" {
                        self.highscorePause = SKSpriteNode(imageNamed: "highscorehard")
                    }
                    
                    self.highscorePause.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+200)
                    self.highscorePause.zPosition = 4
                    self.highscorePause.xScale = 0.9
                    self.highscorePause.yScale = 0.9
                    self.highLabel.fontSize = 180
                    self.addChild(self.highscorePause)
                    
                    self.menuButton.name = "menu"
                    self.menuButton.zPosition = 4
                    self.menuButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-280)
                    self.menuButton.xScale = 0.8
                    self.menuButton.yScale = 0.8
                    self.addChild(self.menuButton)
                    
                    self.highLabel.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-130)
                    self.highLabel.fontSize = 180
                    self.highLabel.text = "\(self.highscore)"
                    self.highLabel.zPosition = 4
                    self.highLabel.fontColor = #colorLiteral(red: 0.07058823529, green: 0.2470588235, blue: 0.2941176471, alpha: 1)
                    self.addChild(self.highLabel)
                    
                } else {
                self.pauseNodeBackground.position = CGPoint(x:  self.frame.width/2, y: self.frame.height/2)
                self.pauseNodeBackground.size = CGSize(width: self.frame.width/3, height: self.frame.height-500)
                self.pauseNodeBackground.zPosition = 3
                self.addChild(self.pauseNodeBackground)
                
                if self.tipo == "easy" {
                    self.highscorePause = SKSpriteNode(imageNamed: "highscoreeasy")
                } else if self.tipo == "normal" {
                    self.highscorePause = SKSpriteNode(imageNamed: "highscorenormal")
                } else if self.tipo == "hard" {
                    self.highscorePause = SKSpriteNode(imageNamed: "highscorehard")
                }
                
                self.highscorePause.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+250)
                self.highscorePause.zPosition = 4
                self.highscorePause.xScale = 1.2
                self.highscorePause.yScale = 1.2
                self.addChild(self.highscorePause)
                
                
                self.menuButton.name = "menu"
                self.menuButton.zPosition = 4
                self.menuButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-330)
                self.addChild(self.menuButton)
                
                
                self.highLabel.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-170)
                self.highLabel.fontSize = 250
                self.highLabel.text = "\(self.highscore)"
                self.highLabel.zPosition = 4
                self.highLabel.fontColor = #colorLiteral(red: 0.07058823529, green: 0.2470588235, blue: 0.2941176471, alpha: 1)
                self.addChild(self.highLabel)
                }
                
            })

        }
    }
    
    func vibrate()
    {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    func updateHUD() {
        
        if tankLives < 0 {
            tankLives = 0
        }
        if !isGameOver {
            
            livesLabel.text = "Lives: \(tankLives)"
            scoreLabel.text = "Score: \(score)"

        }
    }
    
    
}
    public extension UIDevice {
        
        var modelName: String {
            var systemInfo = utsname()
            uname(&systemInfo)
            let machineMirror = Mirror(reflecting: systemInfo.machine)
            let identifier = machineMirror.children.reduce("") { identifier, element in
                guard let value = element.value as? Int8, value != 0 else { return identifier }
                return identifier + String(UnicodeScalar(UInt8(value)))
            }
            
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
            case "AppleTV5,3":                              return "Apple TV"
            case "i386", "x86_64":                          return "Simulator"
            default:                                        return identifier
            }
        }
        
}

