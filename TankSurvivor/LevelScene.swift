//
//  LevelScene.swift
//  TankSurvivor
//
//  Created by Cesar Castillo on 04/04/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import UIKit
import SpriteKit
import Foundation

class LevelScene: SKScene {
    
    var easyButton = SKSpriteNode(imageNamed: "easybutton")
    var normalButton = SKSpriteNode(imageNamed: "normalbutton")
    var hardButton = SKSpriteNode(imageNamed: "hardbutton")
    var arcadeButton = SKSpriteNode(imageNamed: "arcadebutton")
    var ranking = SKSpriteNode(imageNamed: "ranking")
    var atras = SKSpriteNode(imageNamed: "back")
    var playableArea : CGRect?
    
    
    override init (size: CGSize) {
        
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            if (UIScreen.main.nativeBounds.height == 2436) {
                let maxAspectRatio : CGFloat = 2.17/1.0
                let playableHeight = size.width / maxAspectRatio
                let playableMargin = (size.height - playableHeight) / 2.0
                playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
            } else {
                let maxAspectRatio : CGFloat = 16.0/9.0
                let playableHeight = size.width / maxAspectRatio
                let playableMargin = (size.height - playableHeight) / 2.0
                playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
            }
        } else if UIDevice.current.userInterfaceIdiom == .pad {
            
            let maxAspectRatio : CGFloat = 4.0/3.0
            let playableHeight = size.width / maxAspectRatio
            let playableMargin = (size.height - playableHeight) / 2.0
            
            playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
            
        }

        
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("El metodo init no ha sido implementado")
    }
    
    
    
    override  func didMove(to view: SKView) {
        let background = SKSpriteNode(imageNamed: "levelbackground")
        background.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        background.zPosition = -1
        addChild(background)
        
//        ranking.position = CGPoint(x: size.width-500, y: -size.height+300)
//        ranking.zPosition = 2
//        ranking.xScale = 0.5
//        ranking.yScale = 0.5
//        addChild(ranking)
        
        atras.name = "atrasbutton"
        atras.position = CGPoint(x: self.frame.width/2-850, y: (playableArea?.maxY)!-150)
        atras.zPosition = 3
        atras.xScale = 0.8
        atras.yScale = 0.8
        addChild(atras)
        
        easyButton.position = CGPoint(x: size.width/2, y: size.height/2+300)
        easyButton.zPosition = 2
        easyButton.xScale = 1.5
        easyButton.yScale = 1.5
        easyButton.name = "easybutton"
        addChild(easyButton)
        
        normalButton.position = CGPoint(x: size.width/2, y: size.height/2)
        normalButton.zPosition = 2
        normalButton.xScale = 1.5
        normalButton.yScale = 1.5
        normalButton.name = "normalbutton"
        addChild(normalButton)
        
        hardButton.position = CGPoint(x: size.width/2, y: size.height/2-300)
        hardButton.zPosition = 2
        hardButton.xScale = 1.5
        hardButton.yScale = 1.5
        hardButton.name = "hardbutton"
        addChild(hardButton)
        
        arcadeButton.position = CGPoint(x: size.width/2, y: size.height/4)
        arcadeButton.zPosition = 0
        arcadeButton.xScale = 1.5
        arcadeButton.yScale = 1.5
        arcadeButton.name = "arcadebutton"
        //addChild(arcadeButton)
        
        //backgroundAudioPlayer.stop()
        
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first! as UITouch
        let location = touch.location(in: self)
        let node = self.atPoint(location)
        
        
        if node.name == "atrasbutton" {
            let scaleUP = SKAction.scale(by: 1.2, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            atras.run(fullScale, completion: {
                let menuScene = MainMenuScene(size: self.size)
                menuScene.scaleMode = self.scaleMode
                let transition = SKTransition.push(with: .down, duration: 1.0)
                self.view?.presentScene(menuScene,transition: transition)
                
            })

        
        } else if node.name == "easybutton" {
            
            
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            
            easyButton.run(fullScale, completion: {
                let gameScene = GameScene(size: self.size,tipo:"easy")
                gameScene.scaleMode = self.scaleMode
                let transition = SKTransition.push(with: .down, duration: 1.0)
                self.view?.presentScene(gameScene, transition: transition)
                
                
            })
            
            
            
            
        } else if node.name == "normalbutton" {
            
            
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            
            normalButton.run(fullScale, completion: {
                
                let gameScene = GameScene(size: self.size,tipo:"normal")
                gameScene.scaleMode = self.scaleMode
                let transition = SKTransition.push(with: .down, duration: 1.0)
                self.view?.presentScene(gameScene, transition: transition)
                
            })
            
        } else if node.name == "hardbutton" {
            
            
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            
            hardButton.run(fullScale, completion: {
                
                let gameScene = GameScene(size: self.size,tipo:"hard")
                gameScene.scaleMode = self.scaleMode
                let transition = SKTransition.push(with: .down, duration: 1.0)
                self.view?.presentScene(gameScene, transition: transition)
            })
            
        } else if node.name == "arcadebutton" {
            
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            
            arcadeButton.run(fullScale, completion: {
                
                let gameScene = GameScene(size: self.size,tipo:"arcade")
                gameScene.scaleMode = self.scaleMode
                //let transition = SKTransition.push(with: .down, duration: 1.0)
                self.view?.presentScene(gameScene)
            })
            
        }
        
    }
}


