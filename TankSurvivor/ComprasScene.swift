//
//  ComprasScene.swift
//  TankSurvivor
//
//  Created by Cesar Castillo on 09/08/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import StoreKit
import CoreData

class ComprasScene: SKScene,SKProductsRequestDelegate,SKPaymentTransactionObserver {
    
     var playableArea : CGRect?
     var atras = SKSpriteNode(imageNamed: "back1")
     var imagen = SKSpriteNode(imageNamed: "imagennocomprado")
     var imagenComprado = SKSpriteNode(imageNamed: "Asset27")
     var restore = SKSpriteNode(imageNamed: "restore")
     var nombreItem : SKLabelNode
     var nombreItem2 : SKLabelNode
     var priceItem : SKLabelNode
     var is_iphoneX : Bool?
    
    var storeCollection = [StoreItem]()
    var product = [SKProduct]()
    
    override init (size: CGSize) {

        if UIDevice.current.userInterfaceIdiom == .phone {
            
            if (UIScreen.main.nativeBounds.height == 2436) {
                is_iphoneX = true
                let maxAspectRatio : CGFloat = 2.17/1.0
                let playableHeight = size.width / maxAspectRatio
                let playableMargin = (size.height - playableHeight) / 2.0
                playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
            } else {
                is_iphoneX = false
                let maxAspectRatio : CGFloat = 16.0/9.0
                let playableHeight = size.width / maxAspectRatio
                let playableMargin = (size.height - playableHeight) / 2.0
                playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
            }
        } else if UIDevice.current.userInterfaceIdiom == .pad {
            
            let maxAspectRatio : CGFloat = 4.0/3.0
            let playableHeight = size.width / maxAspectRatio
            let playableMargin = (size.height - playableHeight) / 2.0
            
            playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
            
           

            
        }
        
        self.nombreItem = SKLabelNode(fontNamed: "Army")
        self.nombreItem2 = SKLabelNode(fontNamed: "Army")
        self.priceItem = SKLabelNode(fontNamed: "Army")
        
        super.init(size: size)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("El metodo init no ha sido implementado")
    }
    
    override func didMove(to view: SKView) {
        
          updateStore()
         NotificationCenter.default.addObserver(self, selector:#selector(cambiarImagenComprado), name: NSNotification.Name(rawValue: "cambiarImagen"), object: nil)
        if self.storeCollection.count == 0 {
            
            createItemStore(title: "Tank Survivor Full Version", imageName: "", purchased: false, productIdentifier: "com.Roca.TankSurvivor.RemoveAds")
            
            
            updateStore()
        }
        requestPurchableProduct()
        
        let background = SKSpriteNode(imageNamed: "background")
        background.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        background.zPosition = -1
        addChild(background)
        
        
        atras.name = "atrasbutton"
        atras.position = CGPoint(x: self.frame.width/2-850, y: (playableArea?.maxY)!-150)
        atras.zPosition = 3
        atras.xScale = 0.8
        atras.yScale = 0.8
        addChild(atras)
        
        imagen.name = "comprar"
        
        imagen.zPosition = 2
        
        
        if (is_iphoneX)! {
            imagen.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+200)
            imagenComprado.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+200)
            imagen.xScale = 0.3
            imagen.yScale = 0.3
            imagenComprado.xScale = 0.3
            imagenComprado.yScale = 0.3
            nombreItem.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-200)
            nombreItem2.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-300)
            priceItem.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-100)
            nombreItem.fontSize = 80
            nombreItem2.fontSize = 80
            priceItem.fontSize = 80
        } else {
            imagen.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+230)
            imagenComprado.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+230)
            imagen.xScale = 0.4
            imagen.yScale = 0.4
            imagenComprado.xScale = 0.4
            imagenComprado.yScale = 0.4
            nombreItem.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-250)
            nombreItem2.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-350)
            priceItem.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-150)
            nombreItem.fontSize = 100
            nombreItem2.fontSize = 100
            priceItem.fontSize = 100
        }
        
       
        
        imagenComprado.name = ""
        
        imagenComprado.zPosition = 2
      
        
        
       
        
        restore.name = "restore"
        restore.position = CGPoint(x: self.frame.width/2+850, y:  (playableArea?.maxY)!-150)
        restore.zPosition = 3
        restore.xScale = 0.8
        restore.yScale = 0.8
        addChild(restore)
        
      
        
        
        nombreItem.zPosition = 2
        //nombreItem.text = "el item"
        addChild(nombreItem)
        
        
        
        nombreItem2.zPosition = 2
        //nombreItem.text = "el item"
        addChild(nombreItem2)
        
       

        priceItem.zPosition = 2
        //priceItem.text = "el item"
        addChild(priceItem)
        
         run(SKAction.repeatForever(SKAction.sequence([SKAction.run(animateAtrasButton),SKAction.wait(forDuration: 2.0)])))
         run(SKAction.repeatForever(SKAction.sequence([SKAction.run(animateRestoreButton),SKAction.wait(forDuration: 2.0)])))
        
        let hightscoreDefaults = UserDefaults.standard
        if (hightscoreDefaults.value(forKey: "purchased") != nil) {
            if (hightscoreDefaults.value(forKey: "purchased") as! Bool == true)
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeAds"), object: self)
                print("Si los removio")
                addChild(imagenComprado)
                
            }
        } else {
            
            addChild(imagen)
            
            
            
        }


        

        
    }
    
    func animateAtrasButton() {
        
        let moveUp = SKAction.moveBy(x: 0.0, y: 10, duration: 1.0)
        let moveDown = SKAction.moveBy(x: 0.0, y: -10, duration: 1.0)
        atras.run(SKAction.sequence([moveUp,moveDown]))
    }
    
    func animateRestoreButton() {
        
        let moveUp = SKAction.moveBy(x: 0.0, y: 10, duration: 1.0)
        let moveDown = SKAction.moveBy(x: 0.0, y: -10, duration: 1.0)
        restore.run(SKAction.sequence([moveUp,moveDown]))
    }

    
    
    
    func requestPurchableProduct() {
        
        var productIdentifier = Set<String>()
        
        for StoreItem in self.storeCollection {
            if let myID = StoreItem.productIdentifier {
                productIdentifier.insert(myID)
            }
        }
        
        
        let productRequest = SKProductsRequest(productIdentifiers: productIdentifier)
        productRequest.delegate = self
        productRequest.start()
    }
    
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("recibimos de app store")
        
        print("\(response.products)")
        
        self.product = response.products
        
        let comprasVC = ComprasScene(size: CGSize(width: 0, height: 0))
        comprasVC.product = response.products
        
        for product in response.products {
            print("\(product.localizedTitle) : \(product.price)")
            
           
        }
        
        
        if let storeItem = self.storeCollection.first
        {
            if storeItem.purchased {
                
                //imagen = SKSpriteNode(imageNamed: "tank")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeAds"), object: self)
                priceItem.text = "Bought"
                priceItem.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)
                nombreItem.text = "Tank Survivor"
                nombreItem.fontColor = UIColor(colorLiteralRed: 1.0, green: 1.0, blue: 1.0,alpha: 1.0)
                nombreItem2.text = "Full Version"
                nombreItem2.fontColor = UIColor(colorLiteralRed: 1.0, green: 1.0, blue: 1.0,alpha: 1.0)

            } else {
                
                
                for prod in self.product {
                    if prod.productIdentifier == storeItem.productIdentifier {
                        
                        let formatter = NumberFormatter()
                        formatter.numberStyle = NumberFormatter.Style.currency
                        formatter.locale = prod.priceLocale
                        if let price = formatter.string(from: prod.price) {
                            priceItem.text = "\(price)"
                        }
                        nombreItem.text = "Tank Survivor"
                        nombreItem.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)

                        nombreItem2.text = "Full Version"
                        nombreItem2.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)

                    }
                }
            }
        }
        

        
    }
    
    
    func createItemStore(title: String, imageName:String, purchased:Bool, productIdentifier:String) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let persistentContainer = appDelegate.persistentContainer
        let context = persistentContainer.viewContext
        
        if let entity = NSEntityDescription.entity(forEntityName: "StoreItem", in: context){
            let item = NSManagedObject(entity: entity, insertInto: context) as! StoreItem
            item.title = title
            item.imageName = imageName
            item.purchased = purchased
            item.productIdentifier = productIdentifier
        }
        
        do{
            try context.save()
        }catch{}
        
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions {
            
            
            switch transaction.transactionState {
            case .deferred:
                print("deferred")
                break
            case .failed:
                 print("failed")
                SKPaymentQueue.default().finishTransaction(transaction)
               
                break
            case .purchased:
                 print("purchased")
                removeAds(productIdentifier: transaction.payment.productIdentifier)
                SKPaymentQueue.default().finishTransaction(transaction)
                break
            case .purchasing:
                 print("purchasing")
                break
            case .restored:
                 print("restored")
                removeAds(productIdentifier: transaction.payment.productIdentifier)
                 imagen.removeFromParent()
                 priceItem.text = "Bought"
                 priceItem.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)
                
                 nombreItem.text = "Tank Survivor"
                 nombreItem.fontColor = UIColor(colorLiteralRed: 1.0, green: 1.0, blue: 1.0,alpha: 1.0)
                 nombreItem2.text = "Full Version"
                 nombreItem2.fontColor = UIColor(colorLiteralRed: 1.0, green: 1.0, blue: 1.0,alpha: 1.0)
                SKPaymentQueue.default().finishTransaction(transaction)
                break
            }
        }
        
    }
    
    func removeAds(productIdentifier :String) {
        
        for item in storeCollection {
            if item.productIdentifier == productIdentifier {
                item.purchased = true

                
                let purchaseDefault = UserDefaults.standard
                purchaseDefault.set(item.purchased, forKey: "purchased")
                purchaseDefault.synchronize()


                
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let persistentContainer = appDelegate.persistentContainer
                let context = persistentContainer.viewContext
                
                do {
                    try context.save()
                } catch{}
            }
        }
        
        if let storeItem = self.storeCollection.first
        {
            if storeItem.purchased {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeAds"), object: self)
                imagen.removeFromParent()
                
                priceItem.text = "Bought"
                priceItem.fontColor = UIColor(colorLiteralRed: 18.0/255.0, green: 63.0/255.0, blue: 75.0/255.0, alpha: 1.0)
                addChild(imagenComprado)

            }
        }

        
    }
    
    func updateStore(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let persistentContainer = appDelegate.persistentContainer
        let context = persistentContainer.viewContext
        
        let fetch: NSFetchRequest<StoreItem> = StoreItem.fetchRequest()
        
        do{
            self.storeCollection = try context.fetch(fetch)
        }catch{}
        
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first! as UITouch
        let location = touch.location(in: self)
        let node = self.atPoint(location)
        
        
        if node.name == "atrasbutton" {
            let scaleUP = SKAction.scale(by: 1.2, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            atras.run(fullScale, completion: {
                let menuScene = MainMenuScene(size: self.size)
                menuScene.scaleMode = self.scaleMode
                let transition = SKTransition.push(with: .down, duration: 1.0)
                self.view?.presentScene(menuScene,transition: transition)
                
            })

        } else if node.name == "comprar" {
            
            let scaleUP = SKAction.scale(by: 1.2, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
           
            
            if let storeItem = self.storeCollection.first
            {
                if !storeItem.purchased {
                    
                    imagen.run(fullScale, completion: {
                    for prod in self.product {
                        if prod.productIdentifier == storeItem.productIdentifier {
                            
                            SKPaymentQueue.default().add(self)
                           let payment = SKPayment(product: prod)
                            SKPaymentQueue.default().add(payment)
                        }
                    }
                   })
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeAds"), object: self)
                    imagen = SKSpriteNode(imageNamed: "tank")

                }
            }
            
        } else if node.name == "restore" {
            let scaleUP = SKAction.scale(by: 1.2, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            restore.run(fullScale, completion: {
                SKPaymentQueue.default().add(self)
                SKPaymentQueue.default().restoreCompletedTransactions()
                
                
            })
            
          
        }

        
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        
        
        
    }
    
    func recibir(notification: NSNotification) {
        if let product1 = notification.userInfo?["product"] as? SKProduct {
            
            print("\(product1.localizedTitle)")
        }
    }
    
    
    func cambiarImagenComprado() {
//        imagen.removeFromParent()
//        imagen = SKSpriteNode(imageNamed: "cloud")
//        addChild(imagen)
        
    }


    


}
