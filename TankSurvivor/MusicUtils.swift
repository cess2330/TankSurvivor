//
//  MusicUtils.swift
//  SpaceChimpy
//
//  Created by Cesar Castillo on 10/03/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import Foundation
import AVFoundation


var backgroundAudioPlayer : AVAudioPlayer!

func playBackgroundMusic(filename: String) {
    
    let urlSound = Bundle.main.url(forResource: filename, withExtension: nil)
    if (urlSound == nil) {
        return
    }
    do {
        
    backgroundAudioPlayer =  try AVAudioPlayer(contentsOf: urlSound!)
    if backgroundAudioPlayer == nil {
        return
    }
    
    backgroundAudioPlayer.numberOfLoops = -1
    backgroundAudioPlayer.prepareToPlay()
    backgroundAudioPlayer.play()
    } catch {
        print("Error")
    }
}
