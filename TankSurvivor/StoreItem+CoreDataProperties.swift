//
//  StoreItem+CoreDataProperties.swift
//  Tienda De JB
//
//  Created by Juan Gabriel Gomila Salas on 14/5/17.
//  Copyright © 2017 Juan Gabriel Gomila Salas. All rights reserved.
//

import Foundation
import CoreData


extension StoreItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StoreItem> {
        return NSFetchRequest<StoreItem>(entityName: "StoreItem")
    }

    @NSManaged public var title: String?
    @NSManaged public var imageName: String?
    @NSManaged public var purchased: Bool
    @NSManaged public var productIdentifier: String?

}
